#!/usr/bin/python
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import
import sys
import os
import logging as log
import fontforge

__version__ = "0.1"
try:
    import fuwenlu.constants as const
except ImportError:
    import constants as const
__version__ = const.__version__

def parse_args():
    import argparse
    parser = argparse.ArgumentParser(
            description="Dump unicode glyphs in the given fonts.")
    parser.add_argument("fontfiles", metavar="font-filename", nargs="+",
            help="source font files to dump.")
    parser.add_argument("-u", "--ucode", action="store_true",
            help="show unicode number in hex instead of text")
    parser.add_argument("--version", action="version",
            version="%(prog)s {}".format(__version__))
    args = parser.parse_args()
    return args

def dump_chars(font_filename, hex=False):
    result = set()
    font = fontforge.open(font_filename)
    for g in font.glyphs():
        if g.unicode > 0:
            result.add(g.unicode)
        if g.altuni:
            for alt in g.altuni:
                result.add(alt[0])
    if not hex:
        result_uchar = [unichr(x) for x in sorted(list(result))]
        result_text = "".join(result_uchar)
    else:
        result_uchar = ["0x{:X}".format(x) for x in sorted(list(result))]
        result_text = " ".join(result_uchar)
    print(result_text.encode("UTF-8"))
    print("Total: {}".format(len(result)))

def main():
    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)
    args = parse_args()
    args.fontfiles = [x.decode(const.NATIVE_ENCODING) for x in args.fontfiles]
    font_filenames = args.fontfiles
    for fname in font_filenames:
        print("Font: {}".format(fname))
        dump_chars(fname, args.ucode)

if __name__ == '__main__':
    main()

