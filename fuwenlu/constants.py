#!/usr/bin/python
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import
import os
import sys

prefix = "../"
dist_name = ""
__version__ = '0.2'
data_dir = "../"
try:
    from fuwenlu.site import *
    import fuwenlu.site
    __version__ = fuwenlu.site.dist_version
except ImportError:
    pass

CJK_RANGE = [
        (0x4E00, 0x9FFF),   # BMP
        (0x3400, 0x4DBF),   # ext-A
        (0x20000, 0x2A6DF), # ext-B
        (0x2A700, 0x2B73F), # ext-C
        (0x2B740, 0x2B81F), # ext-D
        (0xF900, 0xFAFF),   # compatible
        (0x2F800, 0x2FA1F), # compatible suppl
        (0x3000, 0x303F),   # CJK puncs
        (0xFE10, 0xFE1F),   # CJK puncs vertical
        (0xFE30, 0xFE4F),   # CJK puncs compatible
        (0xFF00, 0xFFEF),   # fullwidth puncs
        ]

NATIVE_ENCODING = sys.getfilesystemencoding()

