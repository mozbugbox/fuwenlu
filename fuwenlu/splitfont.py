#!/usr/bin/python
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import
import sys
import os
import io
import logging as log

import multiprocessing
import fontforge

__version__ = "0.1"
try:
    import fuwenlu.constants as const
except ImportError:
    import constants as const
__version__ = const.__version__

STEP = 1500
CSS_LINE_MAX = 76

PROFILE_PATH = "profiles"
PROFILE_USER_PATH = os.path.expandvars("$HOME/.config/fuwenlu/profiles")
PROFILE_EXT = ".txt"
DEFAULT_PROFILE = "sogou-freq-all.txt"

CHARSET_PATH = "charsets"
CHARSET_USER_PATH = os.path.expandvars("$HOME/.config/fuwenlu/charsets")
CHARSET_EXT = ".txt"
DEFAULT_CHARSET = "gbk"

CJK_PUNCS = "　。？！，、；：「」『』‘’“”（）〔〕【】—…–．《》〈〉"
CJK_PUNCS_CODE = set([ord(x) for x in CJK_PUNCS])

FONT_FORMATS = [
        "woff",
        "ttf",
        "otf",
        ]

FONT_FACE_FMT = """@font-face {{
  font-family: {};
  src: url({});
  unicode-range: {};
}}
"""

FONT_FACE_LOCAL_FMT = """@font-face {{
  font-family: {};
  src: {};
}}
"""
def load_profile(profile_name):
    """Load from profile
    @return: a list of unicode number from profile"""
    profile_sys = os.path.join(const.data_dir, PROFILE_PATH)
    pathes = [PROFILE_USER_PATH, profile_sys]

    try_list = []
    for path in pathes:
        path_with_ext = "{}{}".format(profile_name, PROFILE_EXT)
        try_list.extend([
                profile_name,
                os.path.join(path, profile_name),
                path_with_ext,
                os.path.join(path, path_with_ext),
                ])

    real_path = ""
    for p in try_list:
        if os.path.isfile(p):
            real_path = p
            break
    log.debug("Use profile {}".format(real_path))
    result = []
    if len(real_path) > 0:
        with io.open(real_path, encoding="UTF-8") as fd:
            for line in fd:
                char = line.split(None, 1)[0]
                ucode = ord(char)
                result.append(ucode)
    result[0:0] = list(CJK_PUNCS_CODE)
    return result

def load_charset(charset_name):
    """Load from charset file
    @return: a list of unicode number from charset file"""
    charset_sys = os.path.join(const.data_dir, CHARSET_PATH)
    pathes = [CHARSET_USER_PATH, charset_sys]

    try_list = []
    for path in pathes:
        path_with_ext = "{}{}".format(charset_name, CHARSET_EXT)
        try_list.extend([
                charset_name,
                os.path.join(path, charset_name),
                path_with_ext,
                os.path.join(path, path_with_ext),
                ])

    real_path = ""
    for p in try_list:
        if os.path.isfile(p):
            real_path = p
            break
    log.debug("Use charset {}".format(real_path))
    result = []
    if len(real_path) > 0:
        with io.open(real_path, encoding="UTF-8") as fd:
            for line in fd:
                if line.isspace(): continue
                char = line[0]
                ucode = ord(char)
                result.append(ucode)
    return result

def load_text_file(text_filename):
    """Load unique unicode from the given UTF-8 text file.
    @return: a list of unicode number"""
    result = set()
    with io.open(text_filename, encoding="UTF-8") as fd:
        for line in fd:
            result.update(line)
    ret = [ord(x) for x in result]
    return ret

def generate_sha1(string_list):
    # Generate a hash from join the glyph names
    one_string = "".join(sorted(string_list))
    import hashlib
    sha = hashlib.sha1()
    sha.update(one_string)
    return sha.hexdigest()

def css_range(ucode_list, consumed_unicode=set()):
    """From a list of unicode point generate a CSS unicode-range field."""
    result = []
    range_start = -1
    range_end = -1

    # join consecutive ucode to U+xxx-yyy
    def range2str(s, e):
        end_mark = ""
        if s != e:
            end_mark = "-{:X}".format(e)
        rtxt = "U+{:X}{}".format(s, end_mark)
        return rtxt

    # join both list to find un-precised ranges
    ucode_list = list(set(ucode_list) | consumed_unicode)

    for ucode in sorted(ucode_list):
        if ucode < 0: continue
        if range_start < 0:
            range_start = ucode
            range_end = ucode
            continue
        if ucode == range_end + 1:
            range_end = ucode
        else:
            # exclude range in the  consumed unicode
            new_range = False
            range_set = set(range(range_start, range_end+1))
            if not (consumed_unicode >= range_set): # no subset
                new_range = True
                used_set = range_set - consumed_unicode
                # used shortist range
                used_list = sorted(used_set)
                range_start = used_list[0]
                range_end = used_list[-1]
            if new_range:
                result.append(range2str(range_start, range_end))

            range_start = ucode
            range_end = ucode

    if range_start >= 0:
        new_range = False
        range_set = set(range(range_start, range_end+1))
        if not (consumed_unicode >= range_set):
            new_range = True
            used_set = range_set - consumed_unicode
            if len(used_set) == 1:
                range_start = range_end = used_set.pop()

        if new_range:
            result.append(range2str(range_start, range_end))

    # wrap lines to CSS_LINE_MAX
    indent = len("  unicode-range: ")
    sub_indent = 4
    text = []
    line = []
    line_max = CSS_LINE_MAX - indent
    n = 0
    for item in result:
        if n + len(item) >= line_max:
            text.append(",".join(line))
            n = 0
            line = []
            # subsqeunt indent set to 4
            if indent != sub_indent:
                indent = sub_indent
                line_max = CSS_LINE_MAX - indent
        line.append(item)
        n += len(item) + 1 # count "," as 1
    if line:
        text.append(",".join(line))

    text = ",\n{}".format(" "*sub_indent).join(text)
    return text

class SplitFont:
    def __init__(self, fname, config):
        self.font_filename = fname
        self.config = config
        self.font = None # fontforge.Font object

        self.fontname = ""
        self.full_fontname = ""

    def collect_refs(self, glyph):
        """Recursively collect all the reference the glyph refered to."""
        result = set()
        refs = glyph.references
        for ref in refs:
            ref_gname = ref[0]
            result.add(ref_gname)
            ref_glyph = glyph.font[ref_gname]
            sub_result = self.collect_refs(ref_glyph)
            result.update(sub_result)
        return result

    def get_char_list(self):
        if self.config.text:
            text_list = load_text_file(self.config.text)
            text_set = set(text_list)

            if not self.config.no_extra:
                char_list = load_profile(self.config.profile)
                for c in char_list:
                    if c not in text_set:
                        text_list.append(c)
            char_list = text_list
        else:
            char_list = load_profile(self.config.profile)

        # Do charset if we have a charset
        if ((not self.config.text or not self.config.no_extra) and
                self.config.charset):
            charset_list = load_charset(self.config.charset)
            char_set = set(char_list)
            for c in charset_list:
                if c not in char_set:
                    char_list.append(c)

        return char_list

    def do_non_cjk(self, unicode_map):
        """If exclude non cjk, remove non cjk code from map.
        @return: new map.
        """
        if not self.config.non_cjk:
            # Only include CJK unicodes
            CJK_RANGE = const.CJK_RANGE
            new_map = {}
            for k in unicode_map.keys():
                iscjk = False
                for r in CJK_RANGE:
                    if r[0] <= k <= r[1]:
                        iscjk = True
                        break
                if iscjk:
                    new_map[k] = unicode_map[k]

            unicode_map = new_map
        return unicode_map

    def collect_cmap(self, font):
        """collect cmap in the font
        @return: a dict map unicode -> a list of glyphnames
        """
        ucode2glyphname = {}
        for glyph in font.glyphs():
            if glyph.unicode >= 0:
                ucode = glyph.unicode
                if ucode not in ucode2glyphname:
                    ucode2glyphname[ucode] = []
                ucode2glyphname[ucode].append(glyph.glyphname)
            if glyph.altuni:
                for alt in glyph.altuni:
                    ucode = alt[0]
                    if ucode not in ucode2glyphname:
                        ucode2glyphname[ucode] = []
                    ucode2glyphname[ucode].append(glyph.glyphname)
        return ucode2glyphname

    def change_step(self, step):
        new_step = step + self.config.increment
        if self.config.max_step > 0 and new_step > self.config.max_step:
            new_step = self.config.max_step
        return new_step

    def calculate_segments(self):
        """Calculate glyphnames in each subfont.
        @return: a list of glyphname set, each form a subfont.
        """
        char_list = self.get_char_list()
        segments = []

        log.debug("Get cmap...")
        font = fontforge.open(self.font_filename)
        self.font = font
        self.fontname = font.fontname
        self.full_fontname = font.fullname

        ucode2glyphname = self.collect_cmap(font)
        ucode2glyphname = self.do_non_cjk(ucode2glyphname)

        ucodes = set(ucode2glyphname.keys())
        step = self.config.step
        seg_glyphname = set()
        used = set()
        duplicated = set() # duplicated in previous subfonts
        def process_glyphname(glyphname):
            """Add glyphname and its unicode references to segment"""
            if glyphname not in used:
                seg_glyphname.add(glyphname)
                used.add(glyphname)
            else:
                return

            # Also add reference for composed glyph
            glyph = font[glyphname]
            sub_glyphnames = self.collect_refs(glyph)
            for sub_gname in sub_glyphnames:
                sub_glyph = font[sub_gname]
                if sub_glyph.unicode >= 0 and sub_gname not in used:
                    seg_glyphname.add(sub_gname)
                    used.add(sub_gname)
                elif sub_gname in used:
                    duplicated.add(sub_gname)

        log.debug("process char_list...")
        # Process unicodes in char_list
        for i, charu in enumerate(char_list):
            if charu not in ucodes: continue # not have it
            glyphnames = ucode2glyphname[charu]
            for aname in glyphnames:
                process_glyphname(aname)
            if len(seg_glyphname) >= step:
                segments.append(seg_glyphname)
                seg_glyphname = set()
                step = self.change_step(step)

        # Process unicodes not in char_list
        if not self.config.no_extra:
            log.debug("process non-char_list...")
            for ucode in sorted(ucode2glyphname.keys()):
                glyphnames = ucode2glyphname[ucode]
                for aname in glyphnames:
                    process_glyphname(aname)
                if len(seg_glyphname) >= step:
                    # include both new glyphnames and duplicated ones.
                    # should make sha1 more sensible
                    segments.append(seg_glyphname | duplicated)
                    seg_glyphname = set()
                    duplicated = set()
                    step = self.change_step(step)

        # push in left overs
        if len(seg_glyphname) > 0:
            segments.append(seg_glyphname)
            seg_glyphname = set()

        #font.close()
        return segments

    def generate_suffixes(self, glyphnames_list):
        """Generate a list of suffixes for the set of glyphnams.
        Sha1 is used to has the glyphnames in each set.
        A sequence number is also used to identify the order of the suffixes.

        @return: ["-01-sha1_hex", "-02-sha1_hex", ...]
        """
        result = []
        import math
        order = math.log(len(glyphnames_list), 10) + 1
        pre_fmt = "{:0%dd}-" % order

        for i, glyphnames in enumerate(glyphnames_list):
            sha1 = generate_sha1(glyphnames)
            pre = pre_fmt.format(i+1) # sequence starts from 1, not 0.
            suffix = "-{}{}".format(pre, sha1)
            result.append(suffix)
        return result

    def generate_subfont_name(self, fname, suffix):
        basename = os.path.basename(fname)
        prename = os.path.splitext(basename)[0]
        subfont_name = "{}{}.{}".format(prename, suffix, self.config.format)
        return subfont_name

    def subfont(self, fname, glyphnames, subfont_name, suffix, sync_list=None):
        """Create sub font in required format based on given glyphnames.
        @sync_list: a multiprocessing.Manager.list() to sync @return
        @return: a set of encoding slot in the font.
        """
        if self.font:
            sfont = self.font
        else:
            sfont = fontforge.open(fname)
        log.info("Source font opened.")
        # collect preserved glyphs
        preserved = set()
        cmap = set()

        for glyph in sfont.glyphs():
            if glyph.glyphname in glyphnames:
                preserved.add(glyph.glyphname)
                ref_gnames = self.collect_refs(glyph)
                preserved.update(ref_gnames)

        log.debug("{} glyphs in subfont.".format(len(preserved)))
        sfont.selection.select(*preserved)
        for g in sfont.selection.byGlyphs:
            if g.unicode > 0:
                cmap.add(g.unicode)
            if g.altuni:
                for alt in g.altuni:
                    cmap.add(alt[0])
        sfont.selection.invert()
        sfont.clear()

        sfont.fontname = sfont.fontname + suffix
        sfont.fullname = sfont.fullname + suffix

        log.debug("generating...")
        gen_flags = []
        sfont.generate(subfont_name, flags=tuple(gen_flags))
        sfont.close()

        if sync_list:
            sync_list.extend(cmap)
        return cmap

    def cut(self):
        log.info("Calculating segments...")
        segs = self.calculate_segments()
        log.info("Creating subfonts in {} format...".format(self.config.format))
        suffixes = self.generate_suffixes(segs)
        sub_font_list = []
        sync_manager = multiprocessing.Manager()

        for i, segi in enumerate(segs):
            suffix = suffixes[i]
            sub_fontname = self.generate_subfont_name(self.font_filename,
                    suffix)
            log.info("[{}/{}] Writing {} ({})...".format(i+1, len(segs),
                sub_fontname, len(segi)))

            if hasattr(os, "fork"):
                # fork a process so we don't have to fontforge reopen the given
                # font.
                sync_cmap = sync_manager.list()
                p = multiprocessing.Process(target=self.subfont, args=(
                    self.font_filename, segi, sub_fontname, suffix, sync_cmap))
                p.start()
                p.join()
                cmap = list(sync_cmap)
            else:
                # without fork, has to always reopen fontforge font
                if self.font:
                    self.font.close()
                    self.font = None
                cmap = self.subfont(self.font_filename, segi,
                        sub_fontname, suffix)
            sub_font_list.append((sub_fontname, cmap))

        self.write_result_information(sub_font_list)

    def write_result_information(self, sub_font_list):
        # Print out the result fonts for copy & paste
        font_text = [x[0] for x in sub_font_list]
        print("Result sub fonts:")
        print("\n".join(font_text).encode("UTF-8"))

        basename = os.path.basename(self.font_filename)
        font_basename = os.path.splitext(basename)[0]
        css_fname = "{}.css".format(font_basename)
        log.info("Writing css @font-face file to {}...".format(css_fname))

        result_text = []
        # FIXME: work around firefox multi-@font-face bug:
        # https://bugzilla.mozilla.org/show_bug.cgi?id=475891
        # each subfont has an uniq font fammily
        result_text_uniq_family = []

        consumed_unicode = set()
        faces = []
        for sub_fontname, urange in sorted(sub_font_list, key=lambda x:x[0]):
            unicode_range_css = css_range(urange, consumed_unicode)
            if not self.config.precise_css_range:
                consumed_unicode.update(urange)

            result_text.append(FONT_FACE_FMT.format(
                font_basename, sub_fontname, unicode_range_css))

            facename = sub_fontname.rsplit("-", 1)[0]
            faces.append(facename)
            result_text_uniq_family.append(FONT_FACE_FMT.format(
                facename, sub_fontname, unicode_range_css))

        result_text.append("/* See: http://dev.w3.org/csswg/css3-fonts/#descdef-unicode-range */")
        result_text.append('@charset "UTF-8";')
        result_text.reverse()

        # Add default local font
        local_faces = []
        if self.full_fontname:
            local_faces.append('local("{}")'.format(self.full_fontname))
        if self.fontname:
            local_faces.append('local("{}")'.format(self.fontname))
        local_face_text = ",\n       ".join(local_faces)

        result_text.append(FONT_FACE_LOCAL_FMT.format(
            font_basename, local_face_text))

        # FIXME: work around firefox multi-@font-face bug:
        result_text_uniq_family.reverse()
        result_text.extend(result_text_uniq_family)
        faces.insert(0, font_basename)
        faces = ['"{}"'.format(x) for x in faces]
        result_text.append(
                "/* sub fonts as indivitual font family. Used for firefox")
        result_text.append("font-family: {};\n*/".format(", ".join(faces)))

        fontname_text = "\n".join(result_text) + "\n"
        with io.open(css_fname, "w", encoding="UTF-8") as fdcss:
            fdcss.write(fontname_text)

def list_profiles():
    print("Available Profiles:")
    profile_sys = os.path.join(const.data_dir, PROFILE_PATH)
    pathes = [PROFILE_USER_PATH, profile_sys]
    ext = PROFILE_EXT
    for path in pathes:
        if not os.path.isdir(path): continue
        piter = sorted(os.listdir(path))
        for p in piter:
            if p.endswith(ext):
                print("  {}".format(p[:-len(ext)]))

def list_charsets():
    print("Available charsets:")
    charset_sys = os.path.join(const.data_dir, CHARSET_PATH)
    pathes = [CHARSET_USER_PATH, charset_sys]
    ext = CHARSET_EXT
    for path in pathes:
        if not os.path.isdir(path): continue
        piter = sorted(os.listdir(path))
        for p in piter:
            if p.endswith(ext):
                print("  {}".format(p[:-len(ext)]))

def parse_args():
    import argparse
    parser = argparse.ArgumentParser(
            description="Split a font into multiple sub fonts. Default to .woff format")
    parser.add_argument("--version", action="version",
            version="%(prog)s {}".format(__version__))

    group_ex = parser.add_mutually_exclusive_group()
    group_ex.add_argument("fontfile", metavar="font-filename", nargs="?",
            help="a source font file to generate sub fonts from.")
    group_ex.add_argument("-l", "--list-profiles", action="store_true",
            help="list available profiles and exit")
    group_ex.add_argument("--list-charsets", action="store_true",
            help="list available charsets and exit")

    parser.add_argument("-p", "--profile", action="store",
            default=DEFAULT_PROFILE,
            help='character frequency profile to use. Default "{}"'.format(DEFAULT_PROFILE))

    parser.add_argument("-c", "--charset", action="store",
            default=DEFAULT_CHARSET,
            help='cjk charset to use. Default "{}"'.format(DEFAULT_CHARSET))

    parser.add_argument("-s", "--step", action="store", type=int, default=STEP,
            help="number of glyhphs in each subfont. Default {}".format(STEP))
    parser.add_argument("--increment", action="store", type=int, default=0,
            help="Progressive increment in each step. Default {}".format(0))
    parser.add_argument("--max-step", action="store", type=int, default=-1,
            help="Max step when increment step. Default no max".format(-1))

    parser.add_argument("-t", "--text", metavar="text-file", action="store",
            help="use characters in the text file.")

    parser.add_argument("--no-extra", action="store_true",
            help="exclude extra glyphs that are not specified")
    parser.add_argument("--non-cjk", action="store_true",
            help="include non-cjk glyphs")
    parser.add_argument("--precise-css-range", action="store_true",
            help="when generate css, provide precise unicode-range instead of overlapping unicode-range of front rules over later rules.")

    parser.add_argument("--format", metavar="output-font-format",
            action="store", default="woff", choices=FONT_FORMATS,
            help="output font format. Default woff")

    parser.add_argument("--debug", action="store_true",
            help="debug run")

    args = parser.parse_args()
    if not (args.list_profiles or args.list_charsets or args.fontfile):
        parser.print_help()
        print("***Error: Need a font filename")
        sys.exit(2)
    return args

def main():
    args = parse_args()
    log_level = log.INFO
    if args.debug:
        log_level = log.DEBUG
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)
    log.debug(args)

    if args.list_profiles:
        list_profiles()
        return
    elif args.list_charsets:
        list_charsets()
        return

    args.fontfile = args.fontfile.decode(const.NATIVE_ENCODING)
    splitf = SplitFont(args.fontfile, args)
    splitf.cut()

    #segs = fcutter.calculate_segments()
    #for i in segs: print(sorted(list(i))[:5])
    #print(segs)

if __name__ == '__main__':
    main()

