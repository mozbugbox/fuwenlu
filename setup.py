#!/usr/bin/python
#vim:fileencoding=utf-8:sw=4
import sys
import os
from distutils.core import setup

__version__ = '0.2'

import shutil
from distutils.dist import Distribution
import distutils.command.install as _install
from distutils.util import subst_vars
from distutils import log

class DistributionLocal(Distribution):
    """Need to subclass in order to add our own class attributes.

    The class attributes defined here can be used in the setup() function
    as parameters. The attributes will be available in other Command
    subclasses as self.distribution.attr_name.

    Need to set 'distclass=DistributionLocal' when call setup().
    """
    def __init__(self, attrs=None):
        self.subst_files = None
        Distribution.__init__(self, attrs)
"""
To print, set env DISTUTILS_DEBUG=1
*** Possible install attributes:
base: /tmp/test-py/
build_base: build
build_lib: build/lib
description: install everything from build directory
dist_fullname: fuwenlu-0.1.0
dist_name: fuwenlu
dist_version: 0.1.0
exec_prefix: /tmp/test-py
extra_dirs:
install_base: /tmp/test-py/
install_data: /tmp/test-py/
install_headers: /tmp/test-py//include/python2.7/fuwenlu
install_lib: /tmp/test-py//lib/python2.7/site-packages/
install_libbase: /tmp/test-py//lib/python2.7/site-packages
install_platbase: /tmp/test-py/
install_platlib: /tmp/test-py//lib/python2.7/site-packages
install_purelib: /tmp/test-py//lib/python2.7/site-packages
install_scripts: /tmp/test-py//bin
install_userbase: /home/userXXX/.local
install_usersite: /home/userXXX/.local/lib/python2.7/site-packages
platbase: /tmp/test-py/
prefix: /tmp/test-py
py_version: 2.7.1
py_version_nodot: 27
py_version_short: 2.7
record: list.txt
sys_exec_prefix: /tmp/test-py
sys_prefix: /tmp/test-py
userbase: /home/userXXX/.local
usersite: /home/userXXX/.local/lib/python2.7/site-packages
*** install attributes end.
"""
class install_local(_install.install):
    """We substitude a 'filename.py.in' to 'filename.py' with
    install time variables. Once the install command is finished, the
    substituded files will be removed automatically.

    Symbols like $prefix will be substitude if possible.

    The substitude candidates should be supplied as 'subst_files' parameter to
    the setup() call.


    setup(subst_files=['filename.py', 'src/file2.py', 'app.desktop'])

    The corresponding '.in' files will be searched and substitude with
    install time variables.
    """

    def initialize_options(self):
        self.subst_files = None
        self.local_vars = None
        _install.install.initialize_options(self)

    def finalize_options(self):
        self.subst_files = self.distribution.subst_files
        _install.install.finalize_options(self)
        self.get_local_vars()

    def run(self):
        self.print_local_vars()
        log.info("subst files {}".format(self.subst_files))
        self.substitude_files()

        _install.install.run(self)

        self.clear_substitude_files()

    def clear_substitude_files(self):
        if self.subst_files is None: return
        for fname in self.subst_files:
            basename = "{}.in".format(fname)
            if os.path.exists(basename) and os.path.exists(fname):
                log.info("removing substitude file {}".format(fname))
                os.remove(fname)

    def substitude_files(self):
        if self.subst_files is None: return
        for fname in self.subst_files:
            basename = "{}.in".format(fname)
            if os.path.exists(basename):
                log.info("substitude {} -> {}".format(basename, fname))
                fin = open(basename)
                text = fin.read()
                fin.close()

                fout = open(fname, "w")
                text_new = subst_vars(text, self.local_vars)
                fout.write(text_new)
                fout.close()

                shutil.copymode(basename, fname)
            else:
                self.warn('cannot substitle {}, no ".in" file found'.format(
                    fname, basename))

    def get_local_vars(self):
        """Collect install time variables from class attributes
        and config_vars"""
        self.local_vars = {}
        for name in dir(self):
            v = getattr(self, name)
            if isinstance(v, str) and (not name.startswith("_")):
                self.local_vars[name] = v
        for k in self.config_vars:
            self.local_vars[k] = self.config_vars[k]

    def print_local_vars(self):
        """print attributes of the class"""
        from distutils.debug import DEBUG
        if DEBUG:
            log.info("*** install attributes:")
            for k in sorted(self.local_vars.keys()):
                log.info("  {}: {}".format(k, self.local_vars[k]))
            log.info("*** install attributes end.")

def do_setup():
    import glob
    setup(name='fuwenlu',
          version=__version__,
          packages=['fuwenlu'],
          scripts=['scripts/fuwenlu-splitfont', "scripts/fuwenlu-dump-text"],
          data_files = [
              #('share/fuwenlu/ui', ['ui/fuwenlu.ui']),
              #('share/applications', ['fuwenlu.desktop']),
              ('share/fuwenlu/profiles', glob.glob("profiles/*.txt")),
              ('share/fuwenlu/charsets', glob.glob("charsets/*.txt")),
              ('share/doc/fuwenlu', ['README', 'TODO', "ChangeLog",
                  "COPYING"]),
              ],
          subst_files = ["fuwenlu/site.py"],

          options={
              "sdist": {
                  "formats": "bztar"
                  },
              },

          author="mozbugbox",
          author_email="mozbugbox@yahoo.com.au",
          url="https://bitbucket.org/mozbugbox/fuwenlu/",
          license="GPL v3.0 or later",
          platforms=["OS Independent"],
          description="Font management tool",
          long_description='''\
Tools to manage font.
''',
          distclass=DistributionLocal,
          cmdclass={"install": install_local}
          )

do_setup()
